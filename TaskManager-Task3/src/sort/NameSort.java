package sort;

import java.util.List;

import models.Task;

public class NameSort implements ISort {

	@Override
	public void filter(List<Task> tasks) {
		tasks.sort((t1, t2) -> t1.getName().compareTo(t2.getName()));;
	}
}
