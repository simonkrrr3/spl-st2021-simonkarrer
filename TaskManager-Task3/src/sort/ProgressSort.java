// #if SortByProgress
package sort;

import java.util.List;

import models.Task;

public class ProgressSort implements ISort {

	@Override
	public void filter(List<Task> tasks) {
		tasks.sort((t1, t2) -> t1.getProgress().compareTo(t2.getProgress()));;
	}
}
// #endif
