// #if SortByDeadline
package sort;

import java.util.List;

import models.Task;

public class DeadlineSort implements ISort {

	@Override
	public void filter(List<Task> tasks) {
		tasks.sort((t1, t2) -> t1.getDeadline().compareTo(t2.getDeadline()));;
	}
}
// #endif
