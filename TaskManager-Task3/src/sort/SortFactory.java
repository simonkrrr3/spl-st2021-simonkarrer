package sort;

public class SortFactory {
	public static ISort create(SortType sortType) {
		switch (sortType) {
		case ByName:
			return new NameSort();
		
		// #if SortByDeadline
		case ByDeadline:
			return new DeadlineSort();
		// #endif
			
		// #if SortByDeadline
		case ByProgress:
			return new ProgressSort();
		// #endif
			
		default:
			return new NameSort();
		
		}
	}
}
