package sort;

public enum SortType {
	ByName,
	
	// #if Deadline
	ByDeadline,
	// #endif
	
	// #if Progress
	ByProgress
	// #endif
}
