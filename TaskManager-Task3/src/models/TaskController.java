package models;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import models.Task;
import sort.ISort;
import sort.SortFactory;
import sort.SortType;

// Singleton
public class TaskController {
	private List<Task> tasks;
	private static TaskController instance = new TaskController();
	
	private TaskController() {
		this.tasks = new ArrayList<Task>();
	}
	
	public static TaskController getInstance() {
		return instance;
	}
	
	
	public void addTask(Task task) {
		// Find highest id of all taks and set new id to task.
		int id = 0;
		for (Task t: this.tasks) {
			if (t.getId() > id) {
				id = t.getId();
			}
		}
		task.setId(++id);
		
		this.tasks.add(task);
	}
	
	public List<Task> getTasks() {
		return this.tasks;
	}
	
	public Task getTaskById(int id) {
		Optional<Task> optionalTask = this.tasks.stream().filter(t -> t.getId() == id).findFirst();
		if (optionalTask.isEmpty()) return null;
		return optionalTask.get();
	}

	public boolean deleteTask(int id) {
		Task task = this.getTaskById(id);
		if (task == null) return false;
	    this.tasks.remove(task);
	    return true;
	}

	public void sortTasks(SortType sortType) {
		ISort filter = SortFactory.create(sortType);
		filter.filter(this.tasks);
	}
}
