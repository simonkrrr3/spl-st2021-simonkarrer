package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.Task;
import models.TaskController;

public class CreateWindow extends JFrame{
	public DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	
	private JButton createButton;
	
	private JTextField nameTextField;
	
	// #if Description
	private JTextField descriptionTextField;
	// #endif
	
	// #if Deadline
	private JTextField deadlineTextField;
	// #endif
	
	public CreateWindow() {
		setSize(1000, 600);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		add(panel);
		
		// Name textField
		panel.add(new JLabel("Name"));
	    this.nameTextField = new JTextField("");
		panel.add(this.nameTextField);
		
		// #if Description
		panel.add(new JLabel("Beschreibung"));
		// Description textField
	    this.descriptionTextField = new JTextField("");
		panel.add(this.descriptionTextField);
		// #endif
		
		// #if Deadline
		panel.add(new JLabel("Deadline"));
		// Deadline textField
	    this.deadlineTextField = new JTextField("");
		panel.add(this.deadlineTextField);
		// #endif
		
		// Create button
		this.createButton = new JButton("Erstellen");
		this.createButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	Task task = new Task(nameTextField.getText());
            	
            	// #if Description
            	task.setDescription(descriptionTextField.getText());
            	// #endif
        		
            	// #if Deadline
            	task.setDeadline(LocalDate.parse(deadlineTextField.getText(), formatter));
            	// #endif
            	
            	TaskController.getInstance().addTask(task);
            	dispose();
            }
        });
		panel.add(this.createButton);
	}
}
