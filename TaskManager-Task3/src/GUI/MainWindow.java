package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;

import models.Task;
import models.TaskController;
import sort.SortType;

public class MainWindow extends JFrame {

	private JList table;
	private JButton updateButton;
	private JButton createButton;
	
	// #if SortByName
	private JButton sortByNameButton;
	// #endif
	
	// #if SortByDeadline
	private JButton sortByDeadlineButton;
	// #endif
	
	// #if SortByProgress
	private JButton sortByProgressButton;
	// #endif
	
	private String[] data;
	
	public MainWindow() {
		setSize(1000, 600);	
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		add(panel);
			
		// List
		List columnNames = this.getColumnNames();
		this.getData(TaskController.getInstance().getTasks());
		this.table = new JList(this.data);
		panel.add(this.table);
		
		// Create button
		this.createButton = new JButton("Erstellen");
		this.createButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CreateWindow createWindow = new CreateWindow();
                createWindow.setVisible(true);
            }
        });
		panel.add(this.createButton);
		
		// Update button
		this.updateButton = new JButton("Update");
		this.updateButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	update();
            }
        });
		panel.add(this.updateButton);
		
		// #if SortByName
		// SortByName button
		this.sortByNameButton = new JButton("SortByName");
		this.sortByNameButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	TaskController.getInstance().sortTasks(SortType.ByName);
            	update();
            }
        });
		panel.add(this.sortByNameButton);
		// #endif
		
		// #if SortByDeadline
		// SortByDeadline button
		this.sortByDeadlineButton = new JButton("SortByDeadline");
		this.sortByDeadlineButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	TaskController.getInstance().sortTasks(SortType.ByDeadline);
            	update();
            }
        });
		panel.add(this.sortByDeadlineButton);
		// #endif
		
		// #if SortByProgress
		// SortByProgress button
		this.sortByProgressButton = new JButton("SortByProgress");
		this.sortByProgressButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	TaskController.getInstance().sortTasks(SortType.ByProgress);
            	update();
            }
        });
		panel.add(this.sortByProgressButton);
		// #endif
	}
	
	private void update() {
		dispose();
    	new MainWindow().setVisible(true);
	}
	
	private List<String> getColumnNames() {
		List<String> columnNames = new ArrayList<>();
		columnNames.add("Name");
		// #if Description
		columnNames.add("Beschreibung");
        // #endif
        // #if Deadline
		columnNames.add("Deadline");
        // #endif
        // #if Progress
		columnNames.add("Progress");
        // #endif
		return columnNames;
	}
	
	private void getData(List<Task> tasks) {
		this.data = new String[tasks.size()];
		
		int i = 0;
		for (Task t : tasks) {
			this.data[i++] = t.print();
		}
	}
}
