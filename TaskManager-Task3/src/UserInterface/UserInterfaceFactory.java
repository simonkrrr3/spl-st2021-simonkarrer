package UserInterface;

public class UserInterfaceFactory {
	public static UserInterface create() {
		// #if CommandLine
		return new CommandLineUserInterface(); 
		// #endif
		
		// #if Graphical
//@		return new GraphicalUserInterface();
		// #endif
	}
}
