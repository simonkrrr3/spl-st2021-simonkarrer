# spl-st2021-simonkarrer

## Task 1

### Model

![Model](model.png)

### Konfigurationen

- Konfiguration 1
    - Alle obligatorischen Features für Command Line Interface

- Konfiguration 2
    - Alle möglichen Features für Command Line Interface

- Konfiguration 3
    - Alle obligatorischen Features für GUI

- Konfiguration 4
    - Auswahl an Features für GUI

- Konfiguration 5
    - Alle möglichen Features für GUI
