package models;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TaskFactory {
	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	
	public static void setProperty(Task task, String[] properties) {
		original(task, properties);
		
		for (String p : properties) {
			if (p.toLowerCase().contains("deadline:")) {
				String deadline = p.replace("deadline:", "");
				task.setDeadline(LocalDate.parse(deadline, formatter));
				return;
			}
		}
	}
}