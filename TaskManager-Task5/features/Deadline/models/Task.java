package models;

import java.time.LocalDate;

/**
 * TODO description
 */
public class Task {
	private LocalDate deadline = LocalDate.now();
	
	public LocalDate getDeadline() {
		return this.deadline;
	}
	
	public void setDeadline(LocalDate deadline) {
		this.deadline = deadline;
	}
	
	public String print() {
		return original() + " - " + this.deadline;
	}
}