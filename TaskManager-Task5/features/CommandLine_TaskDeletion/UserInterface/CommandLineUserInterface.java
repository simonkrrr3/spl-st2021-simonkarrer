package UserInterface;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import models.Progress;
import models.Task;
import models.TaskController;

public class CommandLineUserInterface implements UserInterface {
	
	public void loop(String[] args) {
		original(args);
		
		if (args[0].equals("delete")) {
			if (args.length != 2) {
				System.out.println("Invalid number of arguments for delete");
				return;
			}
			
			if (!TaskController.getInstance().deleteTask(Integer.parseInt(args[1]))) {
				System.out.println("Task couldn't be deleted!");
			}
		}
	}
	
	public void printHelp() {
		original();
		System.out.println(" delete <task_id>");
	}
}
