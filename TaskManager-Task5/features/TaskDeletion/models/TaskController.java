package models;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import models.Task;

// Singleton
public class TaskController {
	public boolean deleteTask(int id) {
		Task task = this.getTaskById(id);
		if (task == null) return false;
	    this.tasks.remove(task);
	    return true;
	}
}
