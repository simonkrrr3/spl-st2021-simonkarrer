package models;

public enum Progress {
	ToDo,
	Doing,
	Done
}
