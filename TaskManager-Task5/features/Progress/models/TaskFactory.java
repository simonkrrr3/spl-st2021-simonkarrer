package models;

public class TaskFactory {
	public static void setProperty(Task task, String[] properties) {
		original(task, properties);
		
		for (String p : properties) {
			if (p.toLowerCase().contains("progress:")) {
				String progress = p.replace("progress:", "").toLowerCase();
				
				if (progress.equals("todo")) {
					task.setProgress(Progress.ToDo);
				} else if (progress.equals("doing")) {
					task.setProgress(Progress.Doing);
				} else {
					task.setProgress(Progress.Done);
				}
				
				return;
			}
		}
	}
}