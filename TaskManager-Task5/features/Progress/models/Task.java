package models;

/**
 * TODO description
 */
public class Task {
	private Progress progress = Progress.ToDo;
	
	public Progress getProgress() {
		return this.progress;
	}
	
	public void setProgress(Progress progress) {
		this.progress = progress;
	}
	
	public String print() {
		return original() + " - " + this.progress;
	}
}