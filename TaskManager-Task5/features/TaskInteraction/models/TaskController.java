package models;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import models.Task;

// Singleton
public class TaskController {
	private List<Task> tasks;
	private static TaskController instance = new TaskController();
	
	private TaskController() {
		this.tasks = new ArrayList<Task>();
	}
	
	public static TaskController getInstance() {
		return instance;
	}
	
	public List<Task> getTasks() {
		return this.tasks;
	}
	
	public Task getTaskById(int id) {
		for (Task t : this.tasks) {
			if (t.getId() == id) 
				return t;
		}
		return null;
	}
}
