
import sort.SortFactory;

public class MainWindow extends JFrame {
	
	private void initialize() {		
		original();
		
		// Add buttons for sorting purposes
		for (String sortType : SortFactory.getAll()) {
			JButton sortButton = new JButton("SortBy" + sortType);
			sortButton.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e) {
	            	TaskController.getInstance().sortTasks(sortType);
	            	update();
	            }
	        });
			panel.add(sortButton);
		}
	}
}
