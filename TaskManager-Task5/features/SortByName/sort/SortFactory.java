package sort;

public class SortFactory {
	public static ISort create(String sortType) {		
		if (sortType.equals("name")) {
			return new NameSort();
		}
		
		return original(sortType);
	}
	
	public static List<String> getAll() {
		List<String> list = original();
		list.add("name");
		return list;
	}
}
