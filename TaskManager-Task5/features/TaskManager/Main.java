import UserInterface.UserInterface;
import UserInterface.UserInterfaceFactory;
import models.Task;
import models.TaskController;

public class Main {

	public static void main(String[] args) {		
		UserInterface ui = UserInterfaceFactory.create();
		ui.run();
	}
}
