package models;

import sort.ISort;
import sort.SortFactory;

public class TaskController {
	public void sortTasks(String sortType) {
		ISort filter = SortFactory.create(sortType);
		filter.filter(this.tasks);
	}
}