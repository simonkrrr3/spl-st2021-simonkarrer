package sort;

import java.util.List;

import models.Task;

public interface ISort {
	void filter(List<Task> tasks);
	
	String getName();
}