package models;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import models.Task;

public class TaskController {	
	public void addTask(String input) {
		input = input.replace(" ", "");
		Task task = new Task();
		TaskFactory.setProperty(task, input.split(";"));
		
		// Find highest id of all tasks and set new id to task.
		int id = 0;
		for (Task t: this.tasks) {
			if (t.getId() > id) {
				id = t.getId();
			}
		}
		task.setId(++id);
		
		this.tasks.add(task);
	}
}
