public class MainWindow extends JFrame {

	private JButton createButton;
	
	private void initialize() {
		original();
		
		// Create button
		this.createButton = new JButton("Erstellen");
		this.createButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CreateWindow createWindow = new CreateWindow();
                createWindow.setVisible(true);
            }
        });
		panel.add(this.createButton);
	}
}
