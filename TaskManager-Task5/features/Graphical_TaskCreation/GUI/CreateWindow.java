package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.Task;
import models.TaskController;

public class CreateWindow extends JFrame{	
	private JButton createButton;
	
	public CreateWindow() {
		setSize(1000, 600);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		add(panel);
		
		// Input textField
		panel.add(new JLabel("Eingabe"));
	    JTextField inputField = new JTextField("");
		panel.add(inputField);
		
		// Create button
		this.createButton = new JButton("Erstellen");
		this.createButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	TaskController.getInstance().addTask(inputField.getText());
            	dispose();
            }
        });
		panel.add(this.createButton);
	}
}
