import java.util.List;

import models.Task;
import models.TaskController;
import sort.ISort;
import sort.SortFactory;

public class CommandLineUserInterface implements UserInterface {
	
	public void printHelp() {
		original();
		
		System.out.print(" [-s <sort_type>], e.g. list -s ");
		
		for (String sortType : SortFactory.getAll()) {
			System.out.print(sortType + ", ");
		}
		
		System.out.println();
	}
	
	private void listTasks(String[] arguments) {
		System.out.println("Tasks (" + TaskController.getInstance().getTasks().size() + ") ");
		
		List<Task> tasks = TaskController.getInstance().getTasks();
		
		if (arguments.length >= 3 && arguments[1].equals("-s")) {
			TaskController.getInstance().sortTasks(arguments[2]);
		}
		
		for (Task t : tasks) {
			System.out.println(" " + t.print());
		}
	}
}
