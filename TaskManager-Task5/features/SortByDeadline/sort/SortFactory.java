package sort;
import java.util.List;
import java.util.ArrayList;

public class SortFactory {
	public static ISort create(String sortType) {		
		if (sortType.equals("deadline")) {
			return new DeadlineSort();
		}
		
		return original(sortType);
	}
	
	public static List<String> getAll() {
		List<String> list = original();
		list.add("deadline");
		return list;
	}
}
