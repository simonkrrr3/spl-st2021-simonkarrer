package UserInterface;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import models.Progress;
import models.Task;
import models.TaskController;

public class CommandLineUserInterface implements UserInterface {

	public DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	
	public void loop(String[] args) {
		original(args);
		
		if (args[0].equals("create")) {
			System.out.println("Folgendes Format eingeben: ");
			Scanner input = new Scanner(System.in);
			TaskController.getInstance().addTask(input.nextLine());
		}
	}
	
	public void printHelp() {
		original();
		System.out.println(" create");
	}
}
