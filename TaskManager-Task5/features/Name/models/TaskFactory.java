package models;

public class TaskFactory {
	public static void setProperty(Task task, String[] properties) {
		original(task, properties);
		
		for (String p : properties) {
			if (p.toLowerCase().contains("name:")) {
				String name = p.replace("name:", "");
				task.setName(name);
				return;
			}
		}
	}
}