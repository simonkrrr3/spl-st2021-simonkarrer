package UserInterface;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import models.Progress;
import models.Task;
import models.TaskController;

public class CommandLineUserInterface implements UserInterface {
	
	public void loop(String[] args) {
		original(args);
		
		if (args[0].equals("edit")) {
			if (args.length != 2) {
				System.out.println("Invalid number of arguments for edit");
				return;
			}
			
			int id = Integer.parseInt(args[1]);
			Task task = TaskController.getInstance().getTaskById(id);
			
			System.out.println("Folgendes Format eingeben: ");
			Scanner input = new Scanner(System.in);
			TaskController.getInstance().editTask(task, input.nextLine());
		}
	}
	
	public void printHelp() {
		original();
		System.out.println(" edit <task_id>");
	}
}
