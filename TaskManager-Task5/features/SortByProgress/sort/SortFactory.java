package sort;

public class SortFactory {
	public static ISort create(String sortType) {		
		if (sortType.equals("progress")) {
			return new ProgressSort();
		}
		
		return original(sortType);
	}
	
	public static List<String> getAll() {
		List<String> list = original();
		list.add("progress");
		return list;
	}
}
