package UserInterface;

public class UserInterfaceFactory {
	public static UserInterface create() {
		return new GraphicalUserInterface();
	}
}
