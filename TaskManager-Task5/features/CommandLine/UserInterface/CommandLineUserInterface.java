package UserInterface;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import models.Task;
import models.TaskController;

public class CommandLineUserInterface implements UserInterface {
	
	public CommandLineUserInterface() { }
	
	@Override
	public void run() {
		Scanner input = new Scanner(System.in);
		System.out.println("TaskManager Input: ");
		
		while (true) {
			System.out.print("\n> ");
			
			String[] arguments = input.nextLine().split(" ");
			
			if (arguments.length == 0) {
				System.out.println("Invalid number of arguments");
			}
			
			if (arguments[0].equals("help")) {
				printHelp();
			}
			
			if (arguments[0].equals("list")) {
				listTasks(arguments);
			}
			
			loop(arguments);
		}
	}
	
	public void loop(String[] args) {}

	private void listTasks(String[] arguments) {
		System.out.println("Tasks (" + TaskController.getInstance().getTasks().size() + ") ");
		
		List<Task> tasks = TaskController.getInstance().getTasks();
		
		for (Task t : tasks) {
			System.out.println(" " + t.print());
		}
	}
	
	public void printHelp() {
		System.out.println("Help:");
		
		System.out.print(" list");
		
		System.out.println();
	}
}
