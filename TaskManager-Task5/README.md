# spl-st2021-simonkarrer

## Task 5

### Benutzung

1. Starten der Anwendung (Command Line Interface)
    Startmethode in der Main-Klasse.
2. Manual des Programms aufrufbar über Eingabe von 'help'
    - create
    - list [-s <sort_type>], e.g. list -s name (Verfügbar, wenn Feature SortByName ausgewählt ist, ansonsten nach ID sortierte Liste)
    - edit <task_id> (Verfügbar, wenn Feature TaskEditing ausgewählt ist)
    - delete <task_id> (Verfügbar, wenn Feature TaskDeletion ausgewählt ist)
    - export (Export in Ascii)


### Feature-Interaktionen
Feature-Interaktionen treten auf, sodass neue Komponenten/Features zwischen zwei interagierenden Featrues erstellt werden müssen. Diese treten vor allem im Bezug auf die User-Interfaces Graphical and CommandLine auf.

- CommandLine_TaskCreation
- CommandLine_TaskEditing
- CommandLine_TaskDeletion
- CommandLine_Sorting
- Graphical_TaskCreation
- Graphical_TaskEditing
- Graphical_TaskDeletion
- Graphical_Sorting

### Implementierte Features sind im folgenden Modell abstrakt markiert

![Implementierte Features Modell](implemented_features_model.png)
*Abstrakte Features sind implementiert*


### Konfigurationen
Im folgenden werden alle Konfigurationen aufgelistet. Zu jeder Konfiguration werden nur alle implementierten Featues aufgelistet.

- Konfiguration 1 (Basis mit ausschließlich Mandatory Features)
    - Command Line
    - CommandLine_TaskCreation
    - TaskCreation
    - Name

- Konfiguration 2 (Zusatz Description, SortByName, TaskEditing und TaskDeletion)
    - Command Line
    - CommandLine_TaskCreation
    - CommandLine_TaskEditing
    - CommandLine_TaskDeletion
    - CommandLine_Sorting
    - TaskCreation
    - TaskEditing
    - TaskDeletion
    - Name
    - Deadline
    - Description
    - SortByName

- Konfiguration 3
    - Graphical
    - Graphical_TaskCreation
    - Graphical_Sorting
    - TaskCreation
    - Name
    - Deadline
    - SortByName
    - SortByDeadline

- Konfiguration 4
    - Graphical
    - TaskCreation
    - Graphical_TaskCreation
    - Graphical_Sorting
    - Name
    - Progress
    - Description
    - Deadline
    - SortByName
    - SortByDeadline
    - SortByProgress
    - ExportAscii

- Konfiguration 5
    - Graphical
    - Graphical_TaskCreation
    - Graphical_TaskEditing
    - Graphical_TaskDeletion
    - Graphical_Sorting
    - TaskCreation
    - Name
    - Description
    - Deadline