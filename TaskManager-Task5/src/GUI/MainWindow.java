package GUI; 

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.util.ArrayList; 
import java.util.List; 

import javax.swing.BoxLayout; 
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JList; 
import javax.swing.JPanel; 

import models.Task; 
import models.TaskController; public   class  MainWindow  extends JFrame {
	

	private JList table;

	
	private JButton updateButton;

	
	private JPanel panel;

	
	
	
	private String[] data;

	
	
	public MainWindow() {
		this.initialize();
	}

	
		
	
	 private void  initialize__wrappee__Graphical  () {
		setSize(1000, 600);	
		
		this.panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		add(panel);
			
		// List
		List columnNames = this.getColumnNames();
		this.getData(TaskController.getInstance().getTasks());
		this.table = new JList(this.data);
		panel.add(this.table);
		
		// Update button
		this.updateButton = new JButton("Update");
		this.updateButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	update();
            }
        });
		panel.add(this.updateButton);
	}

	
	
	private void initialize() {
		initialize__wrappee__Graphical();
		
		// Create button
		this.createButton = new JButton("Erstellen");
		this.createButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CreateWindow createWindow = new CreateWindow();
                createWindow.setVisible(true);
            }
        });
		panel.add(this.createButton);
	}

	
	
	private void update() {
		dispose();
    	new MainWindow().setVisible(true);
	}

	
	
	private List<String> getColumnNames() {
		List<String> columnNames = new ArrayList<String>();
		columnNames.add("Name");
		// #if Description
		columnNames.add("Beschreibung");
        // #endif
        // #if Deadline
		columnNames.add("Deadline");
        // #endif
        // #if Progress
		columnNames.add("Progress");
        // #endif
		return columnNames;
	}

	
	
	private void getData(List<Task> tasks) {
		this.data = new String[tasks.size()];
		
		int i = 0;
		for (Task t : tasks) {
			this.data[i++] = t.print();
		}
	}

	

	private JButton createButton;


}
