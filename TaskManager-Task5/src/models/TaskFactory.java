package models; 

import java.time.LocalDate; 
import java.time.format.DateTimeFormatter; 

public   class  TaskFactory {
	
	 private static void  setProperty__wrappee__TaskCreation  (Task task, String[] properties) {}

	
	 private static void  setProperty__wrappee__Name  (Task task, String[] properties) {
		setProperty__wrappee__TaskCreation(task, properties);
		
		for (String p : properties) {
			if (p.toLowerCase().contains("name:")) {
				String name = p.replace("name:", "");
				task.setName(name);
				return;
			}
		}
	}

	
	 private static void  setProperty__wrappee__Description  (Task task, String[] properties) {
		setProperty__wrappee__Name(task, properties);
		
		for (String p : properties) {
			if (p.toLowerCase().contains("description:")) {
				String desc = p.replace("description:", "");
				task.setDescription(desc);
				return;
			}
		}
	}

	
	
	public static void setProperty(Task task, String[] properties) {
		setProperty__wrappee__Description(task, properties);
		
		for (String p : properties) {
			if (p.toLowerCase().contains("deadline:")) {
				String deadline = p.replace("deadline:", "");
				task.setDeadline(LocalDate.parse(deadline, formatter));
				return;
			}
		}
	}

	
	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");


}
