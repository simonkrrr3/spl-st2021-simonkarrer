package models; 

import java.time.LocalDate; 

/**
 * TODO description
 */
public   class  Task {
	
	private int id;

	

	public void setId(int id) {
		this.id = id;
	}

	
	
	public int getId() {
		return this.id;
	}

	
	
	 private String  print__wrappee__TaskInformation  () {
		return "";
	}

	
	
	 private String  print__wrappee__Name  () {
		return print__wrappee__TaskInformation() + " - " + this.name;
	}

	
	
	 private String  print__wrappee__Description  () {
		return print__wrappee__Name() + " - " + this.description;
	}

	
	
	public String print() {
		return print__wrappee__Description() + " - " + this.deadline;
	}

	
	private String name;

	
	
	public String getName() {
		return this.name;
	}

	
	
	public void setName(String name) {
		this.name = name;
	}

	
	private String description;

	
	
	public String getDescription() {
		return this.description;
	}

	
	
	public void setDescription(String description) {
		this.description = description;
	}

	
	private LocalDate deadline = LocalDate.now();

	
	
	public LocalDate getDeadline() {
		return this.deadline;
	}

	
	
	public void setDeadline(LocalDate deadline) {
		this.deadline = deadline;
	}


}
