package models; 
import java.util.ArrayList; 
import java.util.List; 
import java.util.Optional; 

import models.Task; 

// Singleton
public   class  TaskController {
	
	private List<Task> tasks;

	
	private static TaskController instance = new TaskController();

	
	
	private TaskController() {
		this.tasks = new ArrayList<Task>();
	}

	
	
	public static TaskController getInstance() {
		return instance;
	}

	
	
	public List<Task> getTasks() {
		return this.tasks;
	}

	
	
	public Task getTaskById(int id) {
		for (Task t : this.tasks) {
			if (t.getId() == id) 
				return t;
		}
		return null;
	}

		
	public void addTask(String input) {
		input = input.replace(" ", "");
		Task task = new Task();
		TaskFactory.setProperty(task, input.split(";"));
		
		// Find highest id of all tasks and set new id to task.
		int id = 0;
		for (Task t: this.tasks) {
			if (t.getId() > id) {
				id = t.getId();
			}
		}
		task.setId(++id);
		
		this.tasks.add(task);
	}

		
	public void editTask(Task task, String input) {
		TaskFactory.setProperty(task, input.split(";"));
	}

	
	public boolean deleteTask(int id) {
		Task task = this.getTaskById(id);
		if (task == null) return false;
	    this.tasks.remove(task);
	    return true;
	}


}
