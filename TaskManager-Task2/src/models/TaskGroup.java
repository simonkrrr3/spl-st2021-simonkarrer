package models;

import java.util.ArrayList;
import java.util.List;

public class TaskGroup {
	private String name;
	private List<Task> tasks;

	public TaskGroup(String name) {
		this.name = name;
		this.tasks = new ArrayList<Task>();
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public List<Task> getTasks() {
		return this.tasks;
	}
}
