package models;

import properties.PropertyManager;

public class Task {
	private int id;
	private String name;
	private String description;
	
	public Task(int id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public Task(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public Task(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public String print() {
		String str = id + " - " + name;
		
		if (PropertyManager.getProperty("Description")) {
			str += " - " + description;
		}
		
		return str;
	}
}
