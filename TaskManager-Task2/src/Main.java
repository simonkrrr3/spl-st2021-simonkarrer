import java.util.List;
import java.util.Scanner;

import models.Task;
import properties.PropertyManager;
import sort.SortType;

public class Main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		if (PropertyManager.getProperty("CommandLine")) {
			System.out.println("TaskManager Input: ");
			
			while (true) {
				System.out.print("\n> ");
				
				String[] arguments = input.nextLine().split(" ");
				
				if (arguments.length == 0) {
					System.out.println("Invalid number of arguments");
					continue;
				}
				
				if (arguments[0].equals("help")) {
					printHelp();
					continue;
				}
				
				if (arguments[0].equals("create")) {
					createTask();
					continue;
				}
				
				if (arguments[0].equals("list")) {
					listTasks(arguments);
					continue;
				}
				
				if (PropertyManager.getProperty("TaskEditing")) {
					if (arguments[0].equals("edit")) {
						if (arguments.length != 2) {
							System.out.println("Invalid number of arguments for edit");
							continue;
						}
						
						editTask(arguments[1]);
						continue;
					}
				}
				
				if (PropertyManager.getProperty("TaskDeletion")) {
					if (arguments[0].equals("delete")) {
						if (arguments.length != 2) {
							System.out.println("Invalid number of arguments for delete");
							continue;
						}
						
						if (!TaskController.getInstance().deleteTask(Integer.parseInt(arguments[1]))) {
							System.out.println("Task couldn't be deleted!");
						}
						continue;
					}
				}
				
				System.out.println("Invalid argument: " + arguments[0]);
			}
		}
	}
	
	private static void editTask(String str_id) {
		int id = Integer.parseInt(str_id);
		Task task = TaskController.getInstance().getTaskById(id);

		Scanner input = new Scanner(System.in);
		
		System.out.println("Name: ");
		task.setName(input.nextLine());
		
		if (PropertyManager.getProperty("Description")) {
			System.out.println("Description: ");
			task.setDescription(input.nextLine());
		}
	}

	private static void listTasks(String[] arguments) {
		System.out.println("Tasks (" + TaskController.getInstance().getTasks().size() + ") ");
		
		List<Task> tasks = TaskController.getInstance().getTasks();
		
		if (PropertyManager.getProperty("SortByName") && arguments.length >= 3) {
			if (arguments[1].equals("-s") && arguments[2].equals("name")) {
				TaskController.getInstance().sortTasks(SortType.ByName);
			}
		}
		
		for (Task t : tasks) {
			System.out.println(" " + t.print());
		}
	}

	public static void createTask() {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Name: ");
		String name = input.nextLine();
		Task task = new Task(name);
		
		if (PropertyManager.getProperty("Description")) {
			System.out.println("Description: ");
			String description = input.nextLine();
			task.setDescription(description);
		}
		
		TaskController.getInstance().addTask(task);
	}
	
	public static void printHelp() {
		System.out.println("Help:");
		System.out.println(" create");
		
		System.out.print(" list");
		if (PropertyManager.getProperty("SortByName")) {
			System.out.println(" [-s <sort_type>], e.g. list -s name");
		}
		
		if (PropertyManager.getProperty("TaskEditing")) {
			System.out.println(" edit <task_id>");
		}
		
		if (PropertyManager.getProperty("TaskDeletion")) {
			System.out.println(" delete <task_id>");
		}
		
		System.out.println();
	}
}
