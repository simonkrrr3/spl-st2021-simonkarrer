# spl-st2021-simonkarrer

## Task 2

### Benutzung

1. Starten der Anwendung (Command Line Interface)
    Startmethode in der Main-Klasse.
2. Manual des Programms aufrufbar über Eingabe von 'help'
    - create
    - list [-s <sort_type>], e.g. list -s name (Verfügbar, wenn Feature SortByName ausgewählt ist, ansonsten nach ID sortierte Liste)
    - edit <task_id> (Verfügbar, wenn Feature TaskEditing ausgewählt ist)
    - delete <task_id> (Verfügbar, wenn Feature TaskDeletion ausgewählt ist)


### Implementierte Features

- Command Line (Optional)
- TaskCreation (Mandatory)
- TaskEditing (Optional)
- TaskDeletion (Optional)
- Name (Mandatory)
- Description (Optional)
- SortByName (Optional)

![Implementierte Features Modell](implemented_features_model.png)
*Abstrakte Features sind implementiert*


### Konfigurationen
Im folgenden werden alle Konfigurationen aufgelistet, die relevant für die aktuell implementierten Features sind. Zu jeder Konfiguration werden außerdem alle implementierten Featues aufgelistet.

- Konfiguration 1 (Basis mit ausschließlich Mandatory Features)
    - Command Line
    - TaskCreation
    - Name

- Konfiguration 2 (Zusatz Description, SortByName, TaskEditing und TaskDeletion)
    - Command Line
    - TaskCreation
    - TaskEditing
    - TaskDeletion
    - Name
    - Description
    - SortByName

- Konfiguration 3 (Keine Funktion, da Graphical noch nicht implementiert)

- Konfiguration 4 (Keine Funktion, da Graphical noch nicht implementiert)

- Konfiguration 5 (Keine Funktion, da Graphical noch nicht implementiert)


### UML-Diagramm
(implementierte Klassen sind grün gefärbt)

![UML-Diagramm](UML-Diagram.gif)