# spl-st2021-simonkarrer

## Task 4

### Benutzung

1. Starten der Anwendung (Command Line Interface)
    Startmethode in der Main-Klasse.
2. Manual des Programms aufrufbar über Eingabe von 'help'
    - create
    - list [-s <sort_type>], e.g. list -s name (Verfügbar, wenn Feature SortByName ausgewählt ist, ansonsten nach ID sortierte Liste)
    - edit <task_id> (Verfügbar, wenn Feature TaskEditing ausgewählt ist)
    - delete <task_id> (Verfügbar, wenn Feature TaskDeletion ausgewählt ist)
    - export (Export in Ascii)


### Probleme bei folgenden zuvor implementierten Features
Features können nicht über die Black-Box-Technik bzw. Plugins implementiert werden, weil diese zentrale Bestandteil innerhalb von Klassen sind. Diesen können mit dieser Implementierungstechnik leider nicht aufgelöst werden. Daher sind diese fester Bestandteil des Task4-Projekts.

- TaskCreation (Mandatory)
- TaskEditing (Optional)
- TaskDeletion (Optional)
- Name (Mandatory)
- Description (Optional)
- Progress (Optional)
- Deadline (Optional)

### Implementierte Features

- Command Line
- Graphical
- SortByName (Optional)
- SortByDeadline (Optional)
- SortByProgress (Optional)
- ExportAscii (Optional)

![Implementierte Features Modell](implemented_features_model.png)
*Abstrakte Features sind implementiert*


### Konfigurationen
Im folgenden werden alle Konfigurationen aufgelistet. Zu jeder Konfiguration werden nur alle implementierten Featues aufgelistet.

- Konfiguration 1 (Basis mit ausschließlich Mandatory Features)
    - Command Line
    - TaskCreation
    - Name

- Konfiguration 2 (Zusatz Description, SortByName, TaskEditing und TaskDeletion)
    - Command Line
    - TaskCreation
    - TaskEditing
    - TaskDeletion
    - Name
    - Deadline
    - Description
    - SortByName
    - ExportAscii

- Konfiguration 3
    - Graphical
    - TaskCreation
    - Name
    - Deadline
    - SortByName
    - SortByDeadline

- Konfiguration 4
    - Graphical
    - TaskCreation
    - Name
    - Progress
    - Description
    - Deadline
    - SortByName
    - SortByDeadline
    - SortByProgress
    - ExportAscii

- Konfiguration 5
    - Graphical
    - TaskCreation
    - Name
    - Description
    - Deadline