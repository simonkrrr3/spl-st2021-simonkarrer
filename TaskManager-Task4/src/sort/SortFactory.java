package sort;

import java.util.List;

import interfaces.ISort;

public class SortFactory {
	
	private static SortFactory instance = new SortFactory();
	
	public static SortFactory getInstance() {
		return instance;
	}
	
	private List<ISort> sortTypes;
	
	public void setSortTypes(List<ISort> sortTypes) {
		this.sortTypes = sortTypes;
	}
	
	public ISort create(String sortName) {
		for (ISort sort : sortTypes) {
			if (sort.getSortName().toLowerCase().equals(sortName.toLowerCase())) {
				return sort;
			}
		}
		
		return null;
	}
}
