package export;

import java.util.List;

import interfaces.IExport;

public class ExportFactory {
	private static ExportFactory instance = new ExportFactory();
	
	public static ExportFactory getInstance() {
		return instance;
	}
	
	private List<IExport> exportTypes;
	
	public void setExportTypes(List<IExport> exportTypes) {
		this.exportTypes = exportTypes;
	}
	
	public IExport create(String exportName) {
		for (IExport export : exportTypes) {
			if (export.getExportName().toLowerCase().equals(exportName.toLowerCase())) {
				return export;
			}
		}
		
		return null;
	}
}
