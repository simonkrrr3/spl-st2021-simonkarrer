package interfaces;

import java.util.List;

import models.Task;

public interface IExport {
	void export(List<Task> tasks);
	
	String getExportName();
}
