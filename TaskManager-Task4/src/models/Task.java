package models;

import java.time.LocalDate;

public class Task {
	private int id;
	private String name;
	
	public Task(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	// #if Description
	private String description = "";
	
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	// #endif
	
	// #if Deadline
	private LocalDate deadline = LocalDate.now();
	
	public LocalDate getDeadline() {
		return this.deadline;
	}
	
	public void setDeadline(LocalDate deadline) {
		this.deadline = deadline;
	}
	// #endif

	
	// #if Progress
	private Progress progress = Progress.ToDo;
	
	public Progress getProgress() {
		return this.progress;
	}
	
	public void setProgress(Progress progress) {
		this.progress = progress;
	}
	// #endif
	
	public String print() {
		String str = this.id + " - " + this.name;
		
		// #if Description
		str += " - " + this.description;
		// #endif
		
		// #if Deadline
		str += " - " + this.deadline;
		// #endif
		
		// #if Progress
		str += " - " + this.progress;
		// #endif
		
		return str;
	}
}
