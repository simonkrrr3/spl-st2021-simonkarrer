package loader;

import java.util.List;

import interfaces.ISort;
import sort.SortFactory;

public class Main {

	public static void main(String[] args) {
		//List<UserInterface> uis = PluginLoader.load(UserInterface.class);

		List<ISort> sortTypes = PluginLoader.load(ISort.class);
		SortFactory.getInstance().setSortTypes(sortTypes);
	
	}

}
