// #if SortByProgress


import java.util.List;

import interfaces.ISort;
import models.Task;

public class ProgressSort implements ISort {

	@Override
	public void filter(List<Task> tasks) {
		tasks.sort((t1, t2) -> t1.getProgress().compareTo(t2.getProgress()));;
	}

	@Override
	public String getSortName() {
		return "ProgressSort";
	}
}
// #endif
