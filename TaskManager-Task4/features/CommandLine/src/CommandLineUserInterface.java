// #if CommandLine

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import interfaces.UserInterface;
import models.Progress;
import models.Task;
import models.TaskController;

public class CommandLineUserInterface implements UserInterface {

	public DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	
	public CommandLineUserInterface() { }
	
	@Override
	public void run() {
		Scanner input = new Scanner(System.in);
		System.out.println("TaskManager Input: ");
		
		while (true) {
			System.out.print("\n> ");
			
			String[] arguments = input.nextLine().split(" ");
			
			if (arguments.length == 0) {
				System.out.println("Invalid number of arguments");
				continue;
			}
			
			if (arguments[0].equals("help")) {
				printHelp();
				continue;
			}
			
			if (arguments[0].equals("create")) {
				createTask();
				continue;
			}
			
			if (arguments[0].equals("list")) {
				listTasks(arguments);
				continue;
			}
			
			// #if TaskEditing
			if (arguments[0].equals("edit")) {
				if (arguments.length != 2) {
					System.out.println("Invalid number of arguments for edit");
					continue;
				}
				
				editTask(arguments[1]);
				continue;
			}
			// #endif
		
			// #if TaskDeletion
			if (arguments[0].equals("delete")) {
				if (arguments.length != 2) {
					System.out.println("Invalid number of arguments for delete");
					continue;
				}
				
				if (!TaskController.getInstance().deleteTask(Integer.parseInt(arguments[1]))) {
					System.out.println("Task couldn't be deleted!");
				}
				continue;
			}
			// #endif
			
			System.out.println("Invalid argument: " + arguments[0]);
		}
	}
	
	private void editTask(String str_id) {
		int id = Integer.parseInt(str_id);
		Task task = TaskController.getInstance().getTaskById(id);

		Scanner input = new Scanner(System.in);
		
		System.out.println("Name: ");
		task.setName(input.nextLine());
		
		// #if Description
		System.out.println("Description: ");
		task.setDescription(input.nextLine());
		// #endif
			
		// #if Deadline
		System.out.println("Deadline: format<dd.MM.yyyy>");
		task.setDeadline(LocalDate.parse(input.nextLine(), formatter));
		// #endif
		
		// #if Progress
		System.out.println("Progress: ");
		String strProgress = input.nextLine().toLowerCase();
		Progress progress;
		if (strProgress.equals("todo")) {
			progress = Progress.ToDo;
		} else if (strProgress.equals("doing")) {
			progress = Progress.Doing;
		} else {
			progress = Progress.Done;
		}
		task.setProgress(progress);
		// #endif
	}

	private void listTasks(String[] arguments) {
		System.out.println("Tasks (" + TaskController.getInstance().getTasks().size() + ") ");
		
		List<Task> tasks = TaskController.getInstance().getTasks();
		
		if (arguments.length >= 3 && arguments[1].equals("-s")) {
			TaskController.getInstance().sortTasks(arguments[2]);
		}
		
		for (Task t : tasks) {
			System.out.println(" " + t.print());
		}
	}

	public void createTask() {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Name: ");
		String name = input.nextLine();
		Task task = new Task(name);
		
		// #if Description
		System.out.println("Description: ");
		task.setDescription(input.nextLine());
		// #endif
		
		// #if Deadline
		System.out.println("Deadline: format<dd.MM.yyyy>");
		task.setDeadline(LocalDate.parse(input.nextLine(), formatter));
		// #endif
		
		TaskController.getInstance().addTask(task);
	}
	
	public void printHelp() {
		System.out.println("Help:");
		System.out.println(" create");
		
		System.out.print(" list");
		// #if SortByName || SortByDeadline || SortByProgress
		System.out.print(" [-s <sort_type>], e.g. list -s ");
		
		// #if SortByName
		System.out.print("name, ");
		// #endif
		
		// #if SortByDeadline
		System.out.print("deadline, ");
		// #endif
		
		// #if SortByProgress
		System.out.print("progress, ");
		// #endif
		
		System.out.println();
		
		// #endif
		
		// #if TaskEditing
		System.out.println(" edit <task_id>");
		// #endif
		
		// #if TaskDeletion
		System.out.println(" delete <task_id>");
		// #endif

		System.out.println();
	}
}
// #endif
