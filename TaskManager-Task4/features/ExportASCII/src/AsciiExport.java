import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import interfaces.IExport;
import models.Task;

public class AsciiExport implements IExport {

	@Override
	public void export(List<Task> tasks) {
		try {
			FileWriter myWriter = new FileWriter("export.asc");

			myWriter.write("Tasks: ");
			for (Task task : tasks) {
				myWriter.write(task.print() + "\n");
			}
			
			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getExportName() {
		return "AsciiExport";
	}

}
