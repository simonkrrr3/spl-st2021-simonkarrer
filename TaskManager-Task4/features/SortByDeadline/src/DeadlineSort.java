// #if SortByDeadline


import java.util.List;

import interfaces.ISort;
import models.Task;

public class DeadlineSort implements ISort {

	@Override
	public void filter(List<Task> tasks) {
		tasks.sort((t1, t2) -> t1.getDeadline().compareTo(t2.getDeadline()));;
	}

	@Override
	public String getSortName() {
		return "DeadlineSort";
	}
}
// #endif
